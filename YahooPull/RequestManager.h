#pragma once
#include <curl/curl.h>
#include <iostream>
#include <string>

class RequestManager
{

private:
	CURL *curl;
	CURLcode res;
	static size_t WriteCallback(void *contents, size_t size, size_t nmemb, void *userp)
	{

		((std::string*)userp)->append((char*)contents, size * nmemb);
		return size * nmemb;

	}

public:
	RequestManager();
	~RequestManager();
	void readURL(char* url);
	void saveURL(char* url, std::string& returnString);

};