#include "Stock.h"

using namespace std;
using namespace rapidjson;

void replace(string& replaceIn, string replace, string replaceWith)
{

	size_t replacePos = replaceIn.find(replace, 0);
	replaceIn.erase(replacePos, replace.length());
	replaceIn.insert(replacePos, replaceWith);

}

Stock::Stock(string suppliedSymbol): symbol(suppliedSymbol)
{

	// Call updateValues to get an initial value for ask and bid price
	updateValues();

}

Stock::~Stock()
{
}

void Stock::updateValues()
{

	string returnValue;
	// Adjust the url to the supplied stock symbol, then convert to a char*
	string url = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.quotes%20where%20symbol%20in%20(%22";
	url.append(symbol);
	url.append("%22)%0A%09%09&format=json&diagnostics=true&env=http%3A%2F%2Fdatatables.org%2Falltables.env&callback=");
	char* cURL = _strdup(url.c_str());

	// Load data using the Yahoo Finance API
	RequestManager rm;
	rm.saveURL(cURL, returnValue);

	// Load the data returned by the website into a JSON document
	Document d;
	d.Parse(returnValue.c_str());
	
	// Update bid and ask price
	Value& newAsk = d["query"]["results"]["quote"]["Ask"];
	Value& newBid = d["query"]["results"]["quote"]["Bid"];
	ask = stod(newAsk.GetString());
	bid = stod(newBid.GetString());

	save();

}

void Stock::save()
{

	string sqlString = \
		"INSERT INTO Stocks (Symbol, Ask, Bid, Time) "  \
		"VALUES ('%SYMBOL_REPLACE%', %ASK_REPLACE%, %BID_REPLACE%, datetime());";
	replace(sqlString, "%SYMBOL_REPLACE%", symbol);
	replace(sqlString, "%ASK_REPLACE%", to_string(ask));
	replace(sqlString, "%BID_REPLACE%", to_string(bid));
	char* sql = _strdup(sqlString.c_str());
	cout << sql << endl;

	dbManager.writeQuery(sql);	

}