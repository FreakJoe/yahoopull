#include "DatabaseManager.h"


DatabaseManager::DatabaseManager(): fileName("Stocks.db")
{

	rc = sqlite3_open(fileName, &dataBase);	


	// Initially create the table Stocks if it does not already exist
	char* sql = \
		"CREATE TABLE Stocks("  \
		"ID		INTEGER PRIMARY KEY AUTOINCREMENT," \
		"Symbol TEXT NOT NULL," \
		"Ask	REAL," \
		"Bid	REAL," \
		"Time	DATETIME);";

	writeQuery(sql);

}


DatabaseManager::~DatabaseManager()
{

	rc = sqlite3_close(dataBase);

}

void DatabaseManager::writeQuery(const char* query)
{

	rc = sqlite3_exec(dataBase, query, NULL, NULL, NULL);

}
