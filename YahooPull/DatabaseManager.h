#pragma once

#include <iostream>
#include <sqlite3.h>

class DatabaseManager
{

private:
	char* fileName;
	int rc;
	sqlite3* dataBase;

public:
	DatabaseManager();
	~DatabaseManager();
	void writeQuery(const char* query);

};

