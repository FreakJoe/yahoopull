#pragma once
#include <iostream>
#include <string>

#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

#include "RequestManager.h"
#include "DatabaseManager.h"

class Stock
{

private:
	DatabaseManager dbManager;

public:
	std::string symbol;
	double ask;
	double bid;

	Stock(std::string suppliedSymbol);
	~Stock();
	void updateValues();
	void save();

};