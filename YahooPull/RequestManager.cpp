#include "RequestManager.h"

using namespace std;

RequestManager::RequestManager()
{

	curl_global_init(CURL_GLOBAL_DEFAULT);
	curl = curl_easy_init();
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);

}

RequestManager::~RequestManager()
{

	curl_global_cleanup();

}

void RequestManager::readURL(char* url)
{

	curl_easy_setopt(curl, CURLOPT_URL, url);

	res = curl_easy_perform(curl);
	if (res != CURLE_OK)
		cout << curl_easy_strerror(res) << endl;

	curl_easy_cleanup(curl);

}

void RequestManager::saveURL(char* url, string& returnString)
{
	
	curl_easy_setopt(curl, CURLOPT_URL, url);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &returnString);

	res = curl_easy_perform(curl);
	if (res != CURLE_OK)
		cout << curl_easy_strerror(res) << endl;

	curl_easy_cleanup(curl);

}